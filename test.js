const { Context } = require('@vedavaapi/client/dist/context');
const iacls = require('./dist/acls');
const ires = require('./dist/resolver');

const vc = new Context({
    base: 'https://services.vedavaapi.org:8443/api',
    accessToken: 'VrfOSB94NtujRgNDgD6wgoBankx6BS',
});

const pageId = '5dc1466d549591000e5ff4e3';


async function test() {
    const aclsGResp = await iacls.getAclsHierarchy(vc, pageId);
    const { acls_graph: aclsGraph, graph } = aclsGResp;
    const resolvedAclsGraph = ires.resolveAcls({
        aclsGraph, resGraph: graph, resId: pageId,
    });
    return [resolvedAclsGraph, aclsGResp];
}


const ragP = test();
let rag;
let agr;


ragP.then((v) => {
    [rag, agr] = v;
});


let cas = iacls.getConcernedAgentsSet({
    aclsGResp: agr,
});
