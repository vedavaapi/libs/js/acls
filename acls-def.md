# Acls

## Definition

1. `resource`: A primary persisted resource

2. `agent`: who acts upon `resource(s)`. An `User` or a `Team`

3. `control`: A `directive` command, whose objects are `agent(s)`. any of (`grant`, `block`, `revoke`). `grant` and `block` are `primary-control(s)`, which are meaningful standalone for a given resource. `revoke` is for modifying `inheritance` of `grant` control

4. `action`: An eligible action upon a resource. any of (`read`, `updateContent`, `updateLinks`, `updatePermissions`, `delete`, `createChildren`, `createAnnos`)

5. `AgentsSet`: A collection of agents

6. `ActionControls`: On a given `resource`, for an `action`, a mapping from each `control` to an `AgentSet`. represents collection of control directives

7. `Acl`: On a given resource, a mapping from each `action` to an `ActionControls`. represents collection of action-controls directives

8. `inheritance`: On a given `resource`, for an `action` and corresponding `ActionControls`, `AgentsSet(s)` for `primary-control(s)` will be inherited from `resource`'s `parent(s)`, which are linked through either `source` or `target` fields. It's semantics and `resolution` are explained below

9. `permitted`: On a given `resource` for an `action` against given `agent`, resolved boolean truth, whether `agent` is allowed to perform that action on that resource. We resolve that truth from `Acl(s)`. NOTE that `control(s)` are not permissions, but independent directives which will be used to resolve to judgement.

10. `effective-truth-resolution`: A procedure, which resolves a choosen final truth from acls and inheritance, by adhering to every rule defined in `Resoltion` section.

11. `acls-resolution`: An instance of `effective-truth-resolution`, which computes effective `ResolvedAcl`, for a given `resource` and `action(s)`, by resolving entire inheritance tree  # TODO define ResolvedAcl

12. `permission-resolution`: An instance of `effective-truth-resolution`, which resolves a boolean permission(s), for a given `resource` and `action(s)` against given `AgentSet`. It should take consideration of inheritance. It can short-circute flow where appropriate, instead of considering entire inheritance tree

### Resolution

This section defines ordered semantic steps for resolving `permission` for an `action` on a given `resource` against an `agent`. It resolves from `ActionControl`s defined for `resource` and `inheritance` hierarchy. Once it is resolved, next steps will not apply.

Any `effective-truth-resolution(s)` should adhere to every rule. Algorithms can short-circuit inheritance without breaking definition

1. In ideal state, without any acl, no `agent` has `permission` to perform action

2. if `agent` is in `block` `AgentSet` in `resource`'s `Acl` or any of resource's `Acl` in it's `parent-hierarchy`, `agent` has no `permission` to perform action. RESOLVED:false

3. If `agent` is in `grant` `AgentsSet`, he is permitted. RESOLVED:true

4. If `agent` is in `revoke` `AgentsSet`, `inheritance` is revoked, and thus no need to check `inheritance`. RESOLVED:false

5. If `agent` has `permission` for that `action` on `al least one` of `resource`'s immidiate `parent(s)`, then it has too for this `resource`. (resoltion on `parent` resources follows same definition) RESOLVED:true

6. `agent` has no permission. RESOLVED:false
