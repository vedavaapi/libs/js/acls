import { GraphResponse, IGraph } from '@vedavaapi/client/dist/api/objstore/response-models';
import { IContext } from '@vedavaapi/client/dist/context';
import { accounts } from '@vedavaapi/client';
import { IProjection } from '@vedavaapi/client/dist/api/api-param-models';
import { IAclsGraph, getAclsHierarchy, IAgentsSet, getConcernedAgentsSet, IAcl } from './acls';
import { IResolvedAclsGraph, IResourceHierarchy, resolveAcls, getParentResIds, getInResourceHierarchy } from './resolver';
import { AclControl } from './controls';
import { AclAction } from './actions';


export interface IAclUpdateOpDirectives {
    resId: string;
    actions: AclAction[];
    control: AclControl;
    userIds: string[];
    teamIds: string[];
    method: 'post' | 'delete';
}


export class AclsHierarchyHandler {
    public resId!: string;

    public gResp!: GraphResponse;

    public resGraph!: IGraph;

    public aclsGraph!: IAclsGraph;

    private concernedAgentsSet!: IAgentsSet;

    // private usersGraph: IGraph = {};

    // private teamsGraph: IGraph = {};

    public agentsGraph: IGraph = {};

    public resolvedAclsGraph!: IResolvedAclsGraph;

    public resourcesHierarchy!: IResourceHierarchy;

    constructor(resId: string) {
        this.resId = resId;
    }

    private static getAgentQueryParams(jsonClass: string, agentIds: string[]) {
        return {
            selector_doc: {
                jsonClass,
                _id: {
                    $in: agentIds,
                },
            },
            projection: {
                jsonClass: 1, name: 1, title: 1, _id: 1, email: 1, picture: 1, description: 1,
            } as IProjection,
        };
    }

    public async updateAgentsGraph(vc: IContext) {
        const newAgentFilter = (agentId: string) => !this.agentsGraph[agentId];
        const newUserIds = this.concernedAgentsSet.users.filter(newAgentFilter);
        const newTeamIds = this.concernedAgentsSet.teams.filter(newAgentFilter);
        const [usersResp, teamsResp] = await Promise.all([
            accounts.users.Users.get({ vc, params: AclsHierarchyHandler.getAgentQueryParams('User', newUserIds) }),
            accounts.teams.Teams.get({ vc, params: AclsHierarchyHandler.getAgentQueryParams('Team', newTeamIds) }),
        ]);
        [usersResp, teamsResp].forEach((resp) => {
            resp.data.items.forEach((item) => {
                this.agentsGraph[item._id as string] = item;
            });
        });
    }

    public async init(vc: IContext) {
        this.gResp = await getAclsHierarchy(vc, this.resId);
        this.resGraph = this.gResp.graph;
        this.resourcesHierarchy = getInResourceHierarchy(this.resGraph, this.resId) as IResourceHierarchy;
        this.aclsGraph = this.gResp.acls_graph as IAclsGraph;
        this.resolvedAclsGraph = resolveAcls({
            aclsGraph: this.aclsGraph,
            resGraph: this.resGraph,
            resId: this.resId,
        });

        this.concernedAgentsSet = getConcernedAgentsSet({
            aclsGResp: this.gResp,
        });
        await this.updateAgentsGraph(vc);
        this.agentsGraph['*'] = {
            jsonClass: 'User',
            _id: '*',
            name: '',
            picture: 'icon_wildcard.png',
        }; // Special case for * wild cart
    }

    private partialRecomputeResolvedAclGraph(updatedResId: string) {
        // TODO refactor base graph helpers, graph store from vedavaapi-textract
        let resolvedAclsGraphUpdate: IResolvedAclsGraph = {};
        const updatedRes = this.resGraph[updatedResId];
        if (!updatedRes) return;
        const parentResIds = getParentResIds(updatedRes);
        parentResIds.forEach((resId) => {
            const parentResolvedAcl = this.resolvedAclsGraph[resId];
            if (parentResolvedAcl) {
                resolvedAclsGraphUpdate[resId] = parentResolvedAcl;
            }
        });
        resolvedAclsGraphUpdate = resolveAcls({
            aclsGraph: this.aclsGraph,
            resGraph: this.resGraph,
            resId: this.resId,
            preResolvedAclsGraph: resolvedAclsGraphUpdate,
        });
        Object.assign(this.resolvedAclsGraph, resolvedAclsGraphUpdate);
    }

    async syncUpdate(updatedAcl: IAcl) {
        this.aclsGraph[updatedAcl._id] = updatedAcl;
        this.partialRecomputeResolvedAclGraph(updatedAcl._id);
    }
}
