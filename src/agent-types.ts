export enum AclAgentType {
    USERS = 'users',
    TEAMS = 'teams',
}

export const AclAgentTypes = Object.keys(AclAgentType).map((agentType) => AclAgentType[agentType as keyof typeof AclAgentType]);
