export enum AclAction {
    READ = 'read',
    UPDATE_CONTENT = 'updateContent',
    UPDATE_LINKS = 'updateLinks',
    UPDATE_PERMISSIONS = 'updatePermissions',
    DELETE = 'delete',
    CREATE_CHILDREN = 'createChildren',
    CREATE_ANNOS = 'createAnnos',
}

export const AclActions = Object.keys(AclAction).map((action) => AclAction[action as keyof typeof AclAction]);


export const AclActionLabels = {
    [AclAction.READ]: 'Read',
    [AclAction.CREATE_ANNOS]: 'Create annos',
    [AclAction.CREATE_CHILDREN]: 'Create children',
    [AclAction.DELETE]: 'Delete',
    [AclAction.UPDATE_CONTENT]: 'Update content',
    [AclAction.UPDATE_LINKS]: 'Update links',
    [AclAction.UPDATE_PERMISSIONS]: 'Update permissions',
};
