import { objstore, Context } from '@vedavaapi/client';
import { AclAction, AclActions } from './actions';
import { AclControl, AclControls } from './controls';
import { AclAgentType, AclAgentTypes } from './agent-types';

export class AclsError extends Error {
    // ...
}


export interface IAgentsSet {
    [AclAgentType.USERS]: string[];
    [AclAgentType.TEAMS]: string[];
    jsonClass?: 'AgentIds';
    [x: string]: any;
}


export interface IAccessControl {
    [AclControl.GRANT]: IAgentsSet;
    [AclControl.REVOKE]: IAgentsSet;
    [AclControl.BLOCK]: IAgentsSet;
    jsonClass?: 'AccessControl';
    [x: string]: any;
}


export interface IAcl {
    [AclAction.READ]?: IAccessControl;
    [AclAction.UPDATE_CONTENT]?: IAccessControl;
    [AclAction.UPDATE_LINKS]?: IAccessControl;
    [AclAction.UPDATE_PERMISSIONS]?: IAccessControl;
    [AclAction.DELETE]?: IAccessControl;
    [AclAction.CREATE_ANNOS]?: IAccessControl;
    [AclAction.CREATE_CHILDREN]?: IAccessControl;
    jsonClass?: 'Acl';
    [x: string]: any;
}


export interface IAclsGraph {
    [resId: string]: IAcl;
}


export async function getAclsHierarchy(vc: Context, resId: string, init?: any) {
    const gResp = (await objstore.Graph.put({
        vc,
        data: {
            start_nodes_selector: { _id: resId },
            direction: 'referred',
            max_hops: -1,
            include_acls_graph: true,
            traverse_key_filter_maps_list: [{ source: {}, target: {} }],
            json_class_projection_map: { '*': {
                jsonClass: 1, _id: 1, jsonClassLabel: 1, name: 1, title: 1, source: 1, target: 1, 'resolvedPermissions.updatePermissions': 1,
            } },
        },
        init,
    })).data;

    if (!gResp.start_nodes_ids.includes(resId)) {
        throw new AclsError(`resource with _id ${resId}  not found`);
    }
    return gResp;
}


export function getConcernedAgentsSet({ aclsGResp, aclActions = AclActions, aclControls = AclControls, aclAgentTypes = AclAgentTypes }: {
    aclsGResp: objstore.rms.GraphResponse,
    aclActions?: AclAction[];
    aclControls?: AclControl[];
    aclAgentTypes?: AclAgentType[];
}): IAgentsSet {
    const aclsGraph = aclsGResp.acls_graph!;
    const agentsSet = {
        users: new Set(), teams: new Set(),
    };

    Object.keys(aclsGraph).forEach((resId: string) => {
        const acl: IAcl = aclsGraph[resId];
        if (!acl) return;

        aclActions.forEach((action) => {
            const actionAccessControls = acl[action];
            if (!actionAccessControls) return;

            aclControls.forEach((control) => {
                const controlAgentsSet = actionAccessControls[control];
                if (!controlAgentsSet) return;

                aclAgentTypes.forEach((agentType) => {
                    const agentIds = controlAgentsSet[agentType];
                    if (!agentIds) return;

                    agentIds.forEach((agentId) => {
                        agentsSet[agentType].add(agentId);
                    });
                });
            });
        });
    });

    return {
        users: Array.from(agentsSet.users),
        teams: Array.from(agentsSet.teams),
    } as IAgentsSet;
}
