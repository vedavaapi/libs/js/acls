import delve from 'dlv';
import { objstore } from '@vedavaapi/client';
import { IResource } from '@vedavaapi/client/dist/context';
import { IGraph } from '@vedavaapi/client/dist/api/objstore/response-models';
import { IAcl, IAclsGraph } from './acls';
import { AclAgentType, AclAgentTypes } from './agent-types';
import { AclAction, AclActions } from './actions';
import { AclResolvedControl, ACL_RESOLVED_CONTROL_CONTROL_MAP } from './controls';


export interface IAgentPresense {
    hasInOwn?: boolean;
    inheritedFrom?: string;
}


export interface IAgentIdPresenseMap {
    [agentId: string]: IAgentPresense;
}


export interface IInheritedAgentSet {
    [AclAgentType.USERS]: IAgentIdPresenseMap;
    [AclAgentType.TEAMS]: IAgentIdPresenseMap;
}


export interface IActionResolvedAgents {
    [AclResolvedControl.GRANTED]: IInheritedAgentSet;
    [AclResolvedControl.REVOKED]: IInheritedAgentSet; // let's say it as uninherited
    [AclResolvedControl.BLOCKED]: IInheritedAgentSet;
}


export interface IResolvedAcl {
    [AclAction.READ]?: IActionResolvedAgents;
    [AclAction.UPDATE_CONTENT]?: IActionResolvedAgents;
    [AclAction.UPDATE_LINKS]?: IActionResolvedAgents;
    [AclAction.UPDATE_PERMISSIONS]?: IActionResolvedAgents;
    [AclAction.DELETE]?: IActionResolvedAgents;
    [AclAction.CREATE_ANNOS]?: IActionResolvedAgents;
    [AclAction.CREATE_CHILDREN]?: IActionResolvedAgents;
}


export interface IResolvedAclsGraph {
    [_id: string]: IResolvedAcl;
}


export function getParentResIds(res: IResource) {
    const parentAttr = res.source || res.target || [];
    return Array.isArray(parentAttr) ? parentAttr : [parentAttr];
}


export interface IResourceHierarchy {
    resId: string;
    parents: IResourceHierarchy[];
}


export type IResourceHierarchyBranch = string[];


export function getInResourceHierarchy(resGraph: IGraph, resId: string) {
    if (!Object.prototype.hasOwnProperty.call(resGraph, resId)) return null;
    const hierarchy: IResourceHierarchy = {
        resId,
        parents: [],
    };
    const res = resGraph[resId];
    const parentResIds = getParentResIds(res);
    parentResIds.forEach((pResId) => {
        const pHierarchy = getInResourceHierarchy(resGraph, pResId);
        if (!pHierarchy) return;
        hierarchy.parents.push(pHierarchy);
    });
    return hierarchy;
}


export function getResHierarchyMap(hierarchy: IResourceHierarchy) {
    const resHierarchyMap = {
        [hierarchy.resId]: hierarchy,
    };
    if (hierarchy.parents && hierarchy.parents.length) {
        hierarchy.parents.forEach((parentHierarchy) => {
            Object.assign(resHierarchyMap, getResHierarchyMap(parentHierarchy));
        });
    }
    return resHierarchyMap;
}


export function getDefaultResHierarchyBranch(hierarchy: IResourceHierarchy) {
    let defaultBranch: IResourceHierarchyBranch = [hierarchy.resId];
    if (hierarchy.parents && hierarchy.parents.length) {
        defaultBranch = defaultBranch.concat(getDefaultResHierarchyBranch(hierarchy.parents[0]));
    }
    return defaultBranch;
}


// TODO handle cycles
export function resolveAcls(
    {
        aclsGraph, resGraph, resId, aclActions = AclActions, preResolvedAclsGraph = undefined,
    }: {
        aclsGraph: IAclsGraph;
        resGraph: objstore.rms.IGraph;
        resId: string;
        aclActions?: AclAction[];
        preResolvedAclsGraph?: IResolvedAclsGraph;
    },
): IResolvedAclsGraph {
    const resolvedAclsGraph: IResolvedAclsGraph = {};
    if (preResolvedAclsGraph && Object.prototype.hasOwnProperty.call(preResolvedAclsGraph, resId)) {
        resolvedAclsGraph[resId] = preResolvedAclsGraph[resId];
        return resolvedAclsGraph;
    }
    const res = resGraph[resId];
    if (!res) return resolvedAclsGraph;

    const parentResIds = getParentResIds(res);
    parentResIds.forEach((parentResId) => {
        const inheritedResolvedAclsGraph = resolveAcls({
            aclsGraph, resGraph, resId: parentResId, aclActions, preResolvedAclsGraph,
        });
        Object.assign(resolvedAclsGraph, inheritedResolvedAclsGraph);
    });

    const acl: IAcl = aclsGraph[resId] || {};
    const resolvedAcl: IResolvedAcl = {};

    aclActions.forEach((action) => {
        const actionResolvedAgents = {} as unknown as IActionResolvedAgents;
        // differant handling for non primary controls
        actionResolvedAgents.revoked = {} as unknown as IInheritedAgentSet;
        AclAgentTypes.forEach((agentType) => {
            actionResolvedAgents.revoked[agentType] = {};
            delve(acl, [action, ACL_RESOLVED_CONTROL_CONTROL_MAP[AclResolvedControl.REVOKED], agentType], []).forEach((agentId: string) => {
                actionResolvedAgents.revoked[agentType][agentId] = {
                    hasInOwn: true,
                };
            });
        });

        [AclResolvedControl.GRANTED, AclResolvedControl.BLOCKED].forEach((rControl) => {
            const agentsSet = {} as unknown as IInheritedAgentSet;

            AclAgentTypes.forEach((agentType) => {
                const agentIdPresenseMap: IAgentIdPresenseMap = {};
                parentResIds.forEach((parentResId) => {
                    const parentResolvedAcl = resolvedAclsGraph[parentResId];
                    if (!parentResolvedAcl) return;
                    const plAgentIdPresenseMap: IAgentIdPresenseMap = delve(parentResolvedAcl, [action, rControl, agentType], {});
                    Object.assign(agentIdPresenseMap, JSON.parse(JSON.stringify(plAgentIdPresenseMap))); // innheritance
                    Object.keys(plAgentIdPresenseMap).forEach((agentId) => {
                        const plPresense = plAgentIdPresenseMap[agentId];
                        agentIdPresenseMap[agentId] = {
                            hasInOwn: false,
                            inheritedFrom: plPresense.hasInOwn ? parentResId : plPresense.inheritedFrom,
                        };
                    });
                });
                if (rControl === AclResolvedControl.GRANTED) {
                    Object.keys(actionResolvedAgents.revoked[agentType] || {}).forEach((agentId) => {
                        delete agentIdPresenseMap[agentId]; // revoke inherited
                    });
                }
                delve(acl, [action, ACL_RESOLVED_CONTROL_CONTROL_MAP[rControl], agentType], []).forEach((agentId: string) => {
                    const oldPresense = agentIdPresenseMap[agentId];
                    agentIdPresenseMap[agentId] = { hasInOwn: true }; // own
                    if (oldPresense) agentIdPresenseMap[agentId].inheritedFrom = oldPresense.inheritedFrom;
                });
                agentsSet[agentType] = agentIdPresenseMap;
            });
            actionResolvedAgents[rControl] = agentsSet;
        });
        resolvedAcl[action] = actionResolvedAgents;
    });

    resolvedAclsGraph[resId] = resolvedAcl;
    return resolvedAclsGraph;
}
