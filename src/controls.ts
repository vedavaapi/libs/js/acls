export enum AclControl {
    GRANT = 'grant',
    REVOKE = 'revoke',
    BLOCK = 'block',
}

export enum AclResolvedControl {
    GRANTED = 'granted',
    REVOKED = 'revoked',
    BLOCKED = 'blocked',
}


export const ACL_RESOLVED_CONTROL_CONTROL_MAP = {
    [AclResolvedControl.GRANTED]: AclControl.GRANT,
    [AclResolvedControl.BLOCKED]: AclControl.BLOCK,
    [AclResolvedControl.REVOKED]: AclControl.REVOKE,
};


export const AclControls = Object.keys(AclControl).map((action) => AclControl[action as keyof typeof AclControl]);


export const AclResolvedControls = Object.keys(AclResolvedControl).map(
    (action) => AclResolvedControl[action as keyof typeof AclResolvedControl],
);
